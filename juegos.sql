-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: juegos
-- ------------------------------------------------------
-- Server version	8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `empresa_desarrolladora`
--

DROP TABLE IF EXISTS `empresa_desarrolladora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empresa_desarrolladora` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `anioInicio` date NOT NULL,
  `nit` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa_desarrolladora`
--

LOCK TABLES `empresa_desarrolladora` WRITE;
/*!40000 ALTER TABLE `empresa_desarrolladora` DISABLE KEYS */;
INSERT INTO `empresa_desarrolladora` VALUES (1,'Capcom','Capcom Co. es una empresa japonesa desarrolladora y distribuidora de videojuegos.','1977-05-30','JP3218900003'),(2,'Sonic Team','El Sonic Team es un equipo de creación de videojuegos, responsable de haber creado sagas tan conocidas como Sonic the Hedgehog y NiGHTS Into Dreams.... Principalmente, sus integrantes son de nacionalidad japonesa, pero también han pasado norteamericanos.','2000-06-09','nit-sonic-team'),(3,'Nintendo EAD','Nintendo Entertainment Analysis and Development, más conocido como Nintendo EAD, era el equipo interno de desarrollo de videojuegos más famoso y numeroso de Nintendo.','1983-09-01','nit-Nintendo-EAD');
/*!40000 ALTER TABLE `empresa_desarrolladora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `factura` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int DEFAULT NULL,
  `despachado` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_eba1a13a501d3bd6ebb02ffa063` (`userId`),
  CONSTRAINT `FK_eba1a13a501d3bd6ebb02ffa063` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` VALUES (1,2,1),(2,3,1),(3,4,1),(4,3,1),(5,4,1),(6,4,0);
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plataforma`
--

DROP TABLE IF EXISTS `plataforma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plataforma` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plataforma`
--

LOCK TABLES `plataforma` WRITE;
/*!40000 ALTER TABLE `plataforma` DISABLE KEYS */;
INSERT INTO `plataforma` VALUES (1,'Nintendo 64'),(2,'Sega DreamCast'),(3,'xbox'),(4,'PS2'),(5,'PS3'),(6,'PS4'),(7,'PS5'),(8,'Super Nintendo');
/*!40000 ALTER TABLE `plataforma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro`
--

DROP TABLE IF EXISTS `registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registro` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `cantidad` int NOT NULL,
  `precioTotal` int NOT NULL,
  `videoJuegoId` int DEFAULT NULL,
  `facturaId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_f05c24d5edc296c24cacaf7ebf0` (`videoJuegoId`),
  KEY `FK_caeacf94926b62a697b9bd42611` (`facturaId`),
  CONSTRAINT `FK_caeacf94926b62a697b9bd42611` FOREIGN KEY (`facturaId`) REFERENCES `factura` (`id`),
  CONSTRAINT `FK_f05c24d5edc296c24cacaf7ebf0` FOREIGN KEY (`videoJuegoId`) REFERENCES `video_juego` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro`
--

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;
INSERT INTO `registro` VALUES (1,'2022-01-01',1,50000,1,1),(2,'2022-01-01',1,54900,3,1),(3,'2022-01-01',2,109800,3,2),(4,'2022-01-02',2,100000,1,3),(5,'2022-01-02',1,35000,2,3),(6,'2022-01-03',1,50000,1,4),(7,'2022-01-04',2,60000,4,5),(8,'2022-01-04',2,50000,5,5),(9,'2022-01-04',1,50000,1,5),(14,'2022-01-05',3,36000,6,6);
/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin'),(2,'cliente');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nro_identificacion` varchar(255) NOT NULL,
  `nombres` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `celular` varchar(255) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `roleId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_c28e52f758e7bbc53828db92194` (`roleId`),
  CONSTRAINT `FK_c28e52f758e7bbc53828db92194` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'123456','Andrea','Gutierrez','3101231212','andrea@g.com',1),(2,'234567','Pepe','Perez','3001231212','pepe@jaja.com',2),(3,'345678','Pepa','Perez','3201231212','pepa@jojo.com',2),(4,'','Juan','Valdez','3001231212','juan@coffee.co',2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_juego`
--

DROP TABLE IF EXISTS `video_juego`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `video_juego` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `cantidad` int NOT NULL,
  `fechaSalida` date NOT NULL,
  `precio` int NOT NULL,
  `plataformaId` int DEFAULT NULL,
  `empresaDesarrolladoraId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_777a9e3a14bce7f58282e966090` (`plataformaId`),
  KEY `FK_77d837423f9ea06349c476ddfec` (`empresaDesarrolladoraId`),
  CONSTRAINT `FK_777a9e3a14bce7f58282e966090` FOREIGN KEY (`plataformaId`) REFERENCES `plataforma` (`id`),
  CONSTRAINT `FK_77d837423f9ea06349c476ddfec` FOREIGN KEY (`empresaDesarrolladoraId`) REFERENCES `empresa_desarrolladora` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_juego`
--

LOCK TABLES `video_juego` WRITE;
/*!40000 ALTER TABLE `video_juego` DISABLE KEYS */;
INSERT INTO `video_juego` VALUES (1,'Mario Kart',10,'1992-01-01',50000,1,3),(2,'Sonic the Hedgehog',15,'1991-06-21',35000,2,2),(3,'the Legend of Zelda',30,'1986-02-21',54900,8,1),(4,'Lolo 2',12,'1987-06-15',30000,8,3),(5,'Lolo',8,'1986-01-15',25000,8,3),(6,'Metroid 2',55,'1989-08-15',12000,8,3);
/*!40000 ALTER TABLE `video_juego` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-05  4:44:18
