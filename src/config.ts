export const config = {
    auth: {
        secret: "This+15*My*S3kr37",
        time: 3600,
        tokenKey: 'x-inventario-token',
        messages: {
            required: 'Token is required',
            noValid: 'Token is not valid'
        }
    },
    encryp: {
        saltRounds: 11,
        
    }
};
