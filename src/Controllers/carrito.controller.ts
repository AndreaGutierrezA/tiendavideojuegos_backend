import { Request, Response } from 'express'
import { getRepository } from 'typeorm';
import { Factura } from '../entity/Factura';
import { User } from '../entity/User';
import { VideoJuego } from '../entity/VideoJuego';
import { Registro } from '../entity/Registro';

export class CarritoController {

    static getCarritoByUser = async (req: Request, res: Response): Promise<Response> => {
        const user = await getRepository(User).findOne(req.params.id, {
            relations: [
                'facturas',
                'facturas.registros',
                'facturas.registros.videoJuego'
            ]
        });
        if (!user) {
            res.status(404);
        }
        const facturas = user?.facturas.filter(factura => !factura.despachado)
        return res.json(facturas && facturas.length > 0 ? facturas[0] : null);
    }

    static comprarCarrito = async (req: Request, res: Response): Promise<Response> => {
        const carrito = await getRepository(Factura).findOne(req.params.id, {
            relations: [
                'registros',
                'registros.videoJuego'
            ]
        });
        if (carrito && !carrito.despachado) {
            carrito.despachado = true;
            carrito.registros.forEach(async registro => {
                const videoJuego = registro.videoJuego;
                const cantidad = registro.cantidad;
                videoJuego.cantidad -= cantidad;
                await getRepository(VideoJuego).save(videoJuego);
            });
            const results = await getRepository(Factura).save(carrito);
            return res.json(results);
        }
        return res.status(404).json({ msg: 'Carrito de compras no valido' });
    }

    static eliminarRegistro = async (req: Request, res: Response): Promise<Response> => {
        const resp = await getRepository(Registro).delete(req.params.id);
        return res.json(resp);
    }

}


