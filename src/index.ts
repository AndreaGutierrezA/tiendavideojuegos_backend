import 'reflect-metadata';

import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import { createConnection } from 'typeorm';

import carritoRouter from './routes/carrito.routes';
import empresasRouter from './routes/empresas.routes';
import facturasRouter from './routes/facturas.routes';
import plataformasRouter from './routes/plataformas.routes';
import videoJuegosRouter from './routes/videoJuegos.routes';

const app = express();
createConnection();

// middleware
app.use(cors());
app.use(morgan('dev'));
app.use(express.json());

// routes
app.use(carritoRouter);
app.use(empresasRouter);
app.use(facturasRouter);
app.use(plataformasRouter);
app.use(videoJuegosRouter);

app.listen(3001);
console.log('Server ok on the port 3001');