import { Router } from 'express';
import { FacturaController } from '../Controllers/factura.controller'

const router = Router();

router.get('/facturas', FacturaController.getFacturas);

export default router;