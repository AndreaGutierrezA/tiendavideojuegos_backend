import { Router } from 'express';
import { EmpresaController } from '../Controllers/empresa.controller'

const router = Router();

router.get('/empresas', EmpresaController.getEmpresasDesarrolladoras);
router.get('/empresas/:id', EmpresaController.getEmpresaDesarrolladora);
router.get('/empresas/byPlataforma/:id', EmpresaController.getEmpresasDesarrolladorasByPlataforma);

export default router;