import { Router } from 'express';
import { CarritoController } from '../Controllers/carrito.controller'

const router = Router();

router.get('/carrito/byUser/:id', CarritoController.getCarritoByUser);
router.put('/carrito/comprar/:id', CarritoController.comprarCarrito);
router.delete('/carrito/eliminar/:id', CarritoController.eliminarRegistro);

export default router;