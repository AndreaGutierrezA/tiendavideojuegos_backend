import { Router } from 'express';
import { VideoJuegoController } from '../Controllers/videoJuego.controller'

const router = Router();

router.get('/videojuegos', VideoJuegoController.getVideoJuegos);
router.get('/videojuegos/:id', VideoJuegoController.getVideoJuego);
router.post('/videojuegos', VideoJuegoController.createVideoJuego);
router.put('/videojuegos/:id', VideoJuegoController.modifyVideoJuego);
router.get('/videojuegos/byEmpresa/:id', VideoJuegoController.getVideoJuegosByEmpresa);
router.post('/videojuegos/comprar', VideoJuegoController.comprarVideoJuego);


export default router;