import { Router } from 'express';
import { PlataformaController } from '../Controllers/plataforma.controller'

const router = Router();

router.get('/plataformas', PlataformaController.getPlataformas);

export default router;