import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';
import { Role } from './Role';
import { Factura } from './Factura';
@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nro_identificacion: string;

    @Column()
    nombres: string;

    @Column()
    apellidos: string;

    @Column()
    celular: string;

    @Column()
    correo: string;

    @ManyToOne(type => Role, role => role.users)
    role: Role;

    @OneToMany(type => Factura, factura => factura.user)
    facturas: Factura[];

}