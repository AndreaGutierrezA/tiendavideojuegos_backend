import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne } from 'typeorm';
import { Registro } from './Registro';
import { Plataforma } from './Plataforma';
import { EmpresaDesarrolladora } from './EmpresaDesarrolladora';
@Entity()
export class VideoJuego {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    titulo: string;

    @Column()
    cantidad: number;

    @Column('date')
    fechaSalida: string;

    @Column()
    precio: number;

    @OneToMany(type => Registro, registro => registro.videoJuego)
    registros: Registro[];

    @ManyToOne(type => Plataforma, plataforma => plataforma.videoJuegos)
    plataforma: Plataforma;

    @ManyToOne(type => EmpresaDesarrolladora, empresaDesarrolladora => empresaDesarrolladora.videoJuegos)
    empresaDesarrolladora: EmpresaDesarrolladora;

}