import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { VideoJuego } from './VideoJuego';
@Entity()
export class EmpresaDesarrolladora {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    descripcion: string;

    @Column('date')
    anioInicio: string;

    @Column()
    nit: string;

    @OneToMany(type => VideoJuego, videoJuego => videoJuego.empresaDesarrolladora)
    videoJuegos: VideoJuego[];

}