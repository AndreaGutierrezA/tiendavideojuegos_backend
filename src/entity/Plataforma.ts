import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { VideoJuego } from './VideoJuego';
@Entity()
export class Plataforma {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @OneToMany(type => VideoJuego, videoJuego => videoJuego.plataforma)
    videoJuegos: VideoJuego[];
}